FROM node:lts

RUN apt update && apt install -y ca-certificates gnupg wget git &&\
    wget -qO - https://www.mongodb.org/static/pgp/server-3.2.asc | apt-key add - &&\
    echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/3.2 main" | tee /etc/apt/sources.list.d/mongodb-org-3.2.list &&\
    wget http://snapshot.debian.org/archive/debian/20190501T215844Z/pool/main/g/glibc/multiarch-support_2.28-10_amd64.deb &&\
    dpkg -i multiarch-support*.deb &&\
    wget http://snapshot.debian.org/archive/debian/20170705T160707Z/pool/main/o/openssl/libssl1.0.0_1.0.2l-1%7Ebpo8%2B1_amd64.deb &&\
    dpkg -i libssl1.0.0*.deb &&\
    apt update && apt install -y mongodb-org=3.2.22 mongodb-org-server=3.2.22 mongodb-org-shell=3.2.22 mongodb-org-mongos=3.2.22 mongodb-org-tools=3.2.22

WORKDIR /home/node/app

COPY package*.json ./
RUN npm i
COPY . .


VOLUME /data/db /data/configdb
EXPOSE 3000

CMD mongod --fork --logpath /var/log/mongod.log ; node src/index.js
